$(document).ready(function(){
    init();
});

/**
 * Initialize Options Page
 */
var init = function() {
    // Init Tipsy
    $('input').tipsy({gravity: 'w'});
    $('input').change(saveOptions);

    var settings = {
        host: 'https://example.atlassian.net',
        username: 'username',
        password: 'password',
        position: 0
    }

    chrome.storage.sync.get({
        settings: settings
    }, function(items) {
        $('#host').val(items.settings.host);
        $('#username').val(items.settings.username);
        $('#password').val(items.settings.password);
        $('input[name="infoButton"]')[items.settings.position].checked = true;
    });
}

var saveOptions = function() {
    var host = $('#host').val();
    var username = $('#username').val();
    var password = $('#password').val();
    var position = $('input[name="infoButton"]:checked').val();

    var settings = {
        host: host,
        username: username,
        password: password,
        position: position
    }

    chrome.storage.sync.set({
        settings: settings
    }, function() {
        $('#message').text('Options Saved').parent().show();
        window.setTimeout(function() { clearMessage() }, 1500);
    });
}

var clearMessage = function() {
    $('#message').text('').parent().hide();
}